#! /usr/bin/python
#coding:utf-8

import sys,os,subprocess

# main処理
def main():
    # コマンド引数と引数の個数を取得
    argv = sys.argv
    argc = len(argv)

    # 引数の個数が2個以下(プログラム名を含む)ならUsageを出力して終了
    if (argc < 2):
        print "Usage: # %s inputfile" % argv[0]
        quit()

    for inputFile in argv[1:]:
        ffmpeg_convert(inputFile)

# ffmpeg変換処理
def ffmpeg_convert(inputFile):
    # 放送局がMBSかどうか判断する
    isMBS = (inputFile.find("GR16") > 0)

    # 入力ファイルを取得し、basename、ファイル名を取得し、出力ファイル名を作成
    inputBaseName = os.path.basename(inputFile)
    fileName = inputBaseName.split(".")[0]
    outputBaseName = fileName + ".mp4"
    outputFile = "/var/www/epgrec/video/mp4/" + outputBaseName

    # ffmpegの2パスログファイル名を作成
    ffmpegPassLogFile = "/tmp/pass2_" + fileName + ".log"
    ffmpegLogFile = "/var/log/ffmpeg/" + fileName + ".log"
    # ffmpegのpreset位置
    ffmpegPresetFile = "/usr/local/share/ffmpeg/libx264-hq-ts.ffpreset"

    # ファイル名をダブルクォーテーションでくくる
    inputFile = '"' + inputFile + '"'
    outputFile = '"' + outputFile + '"'
    ffmpegPassLogFile = '"' + ffmpegPassLogFile + '"'
    ffmpegLogFile = '"' + ffmpegLogFile + '"'
    ffmpegPresetFile = '"' + ffmpegPresetFile + '"'


    # 1pass目のコマンド作成
    command1  = "ffmpeg "
    command1 += " -i " + inputFile
    command1 += " -threads 2 "
    command1 += " -cmp chroma "
    command1 += " -flags +ilme+ildct "
    command1 += " -deinterlace "
    command1 += " -top -1 "
    command1 += " -fpre " + ffmpegPresetFile
    command1 += " -vcodec libx264 "
    command1 += " -b:v 1280k "
    command1 += " -s hd720 "
    command1 += " -pass 1 "
    command1 += " -passlogfile " + ffmpegPassLogFile
    command1 += " -f mp4 "
    command1 += " -acodec libfaac -ab 128k -ar 48000 -vol 768 "
    command1 += " -y "
    if isMBS:
        command1 += " -vsync 1 -map '#0x111' -map '#0x112' "
    command1 += outputFile + " 2>> " + ffmpegLogFile

    # 2pass目のコマンド作成
    command2  = "ffmpeg "
    command2 += " -i " + inputFile
    command2 += " -threads 2 "
    command2 += " -cmp chroma "
    command2 += " -flags +ilme+ildct "
    command2 += " -deinterlace "
    command2 += " -top -1 "
    command2 += " -fpre " + ffmpegPresetFile
    command2 += " -vcodec libx264 "
    command2 += " -b:v 1280k "
    command2 += " -s hd720 "
    command2 += " -pass 2 "
    command2 += " -passlogfile " + ffmpegPassLogFile
    command2 += " -f mp4 "
    command2 += " -acodec libfaac -ab 128k -ar 48000 -vol 768 "
    command2 += " -y "
    if isMBS:
        command2 += " -vsync 1 -map '#0x111' -map '#0x112' "
    command2 += outputFile + " 2>> " + ffmpegLogFile

    # パスログを消す
    front = ffmpegPassLogFile[:-1]
    rear = ffmpegPassLogFile[-1:]
    repi = "*"
    ffmpegPassLogFile = front + repi + rear

    command3 = "rm -f " + ffmpegPassLogFile

    isDebug = False
    if isDebug:
        print "command1=\n" + command1
        print "command2=\n" + command2
        print "command3=\n" + command3
        print "isMBS=\n" + str(isMBS)
    else:
        subprocess.check_output(command1, shell=True )
        subprocess.call(command2, shell=True )
        subprocess.call(command3, shell=True )

#----main()----
main()
