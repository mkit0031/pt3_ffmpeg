#! /bin/sh
UNENCODE_TS=/var/www/epgrec/video/un_encode_ts/
ENCODING_TS=/var/www/epgrec/video/encoding_ts/
ENCODED_TS=/var/www/epgrec/video/encoded_ts/
FILE=`ls -tr $UNENCODE_TS | head -1`
if [ -f $UNENCODE_TS$FILE ]
then
	IS_USE=`lsof | grep $FILE`
	if [ -z "$IS_USE" ]
	then
		mv $UNENCODE_TS$FILE $ENCODING_TS
		myFFmpeg.py $ENCODING_TS$FILE
		mv $ENCODING_TS$FILE $ENCODED_TS
	fi
fi
